import sys

nx = 1; dx = 1; lx = 1
ny = 1; dy = 1; ly = 1
nz = 1; dz = 1; lz = 1
filename = ''
for k in range(len(sys.argv)-1):
  k = k + 1
  if sys.argv[k] == '-nx':
    nx = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-ny':
    ny = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-nz':
    nz = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-dx':
    dx = float(sys.argv[k+1])
  if sys.argv[k] == '-dy':
    dy = float(sys.argv[k+1])
  if sys.argv[k] == '-dz':
    dz = float(sys.argv[k+1])
  if sys.argv[k] == '-lx':
    lx = float(sys.argv[k+1])
  if sys.argv[k] == '-ly':
    ly = float(sys.argv[k+1])
  if sys.argv[k] == '-lz':
    lz = float(sys.argv[k+1])
  if sys.argv[k] == '-input_prefix':
    filename = sys.argv[k+1] + '.in'
    
f = open(filename,'w')

simulation = """ 
SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW flow
      MODE TH
    /
  /
END

"""
grid = """
GRID
  TYPE structured
  NXYZ """ + str(nx) + " " + str(ny) + " " + str(nz) + """
  DXYZ
   """ + str(dx) + """d0
   """ + str(dy) + """d0
   """ + str(dz) + """d0
  END
END
"""
region = """
REGION all
  COORDINATES
    0.0d0 0.0d0 0.0d0
    """ + str(lx) + "d0 " + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION left_end
  FACE WEST
  COORDINATES
    0.d0 0.d0 0.d0
    0.d0 """ + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION right_end
  FACE EAST
  COORDINATES
    """ + str(lx) + """d0 0.d0 0.d0
    """ + str(lx) + "d0 " + str(ly) + "d0 " + str(lz) + """d0
  /
END
"""
mat_prop = """
MATERIAL_PROPERTY beam
  ID 1
  POROSITY 0.20
  TORTUOSITY 1.d0
  ROCK_DENSITY 2.0d3 kg/m^3
  SPECIFIC_HEAT 100.0 J/kg-C
  THERMAL_CONDUCTIVITY_DRY 1.16 W/m-C
  THERMAL_CONDUCTIVITY_WET 1.16 W/m-C
  SATURATION_FUNCTION default
  PERMEABILITY
    PERM_X 1.d-14
    PERM_Y 1.d-14
    PERM_Z 1.d-14
  /
END
"""
char_curves = """
SATURATION_FUNCTION default
  SATURATION_FUNCTION_TYPE VAN_GENUCHTEN
  RESIDUAL_SATURATION 0.5d-1
  LAMBDA 0.75
  ALPHA 1.d-3
END
"""
eoswater = """
EOS WATER
  # ref. density, ref. pressure, water compressibility
  DENSITY EXPONENTIAL 1000. 101325. 1.d-8
  VISCOSITY CONSTANT 0.864d-3 Pa-s
END
"""
fluid_prop = """
FLUID_PROPERTY
  DIFFUSION_COEFFICIENT 1.d-9
END
"""
linear_solver = """
LINEAR_SOLVER FLOW
  SOLVER DIRECT
  ZERO_PIVOT_TOL 1.d-20
END
"""
strata = """
STRATA
  REGION all
  MATERIAL beam
END
"""
time = """
TIME
  FINAL_TIME 0.20 day
  INITIAL_TIMESTEP_SIZE 1.d-4 day
  MAXIMUM_TIMESTEP_SIZE 0.0001 day at 0.d0 day
END
"""
output = """
OUTPUT
  SNAPSHOT_FILE
    TIMES day 0.01 0.04 0.09 0.12
    NO_PRINT_INITIAL
    FORMAT HDF5
  /
END
"""
flow_cond = """
FLOW_CONDITION initial
  TYPE
    TEMPERATURE dirichlet
    PRESSURE dirichlet
  /
  TEMPERATURE 25.0d0 C
  PRESSURE 101325 Pa
END

FLOW_CONDITION left_end
  TYPE
    FLUX neumann
    TEMPERATURE dirichlet
  /
  TEMPERATURE 25.0 C
  FLUX 0.d0 m/sec
END

FLOW_CONDITION right_end
  TYPE
    TEMPERATURE dirichlet
    FLUX neumann
  /
  TEMPERATURE 25.0d0 C
  FLUX LIST
    # liquid_flux = (9.0d-6)*t [m/sec]
    TIME_UNITS day
    DATA_UNITS m/sec
    INTERPOLATION LINEAR
    #time  #liquid_flux
    0.00d0 0.0d0
    1.00d0 9.0d-6
  /
END
"""
init_cond = """
INITIAL_CONDITION initial
  REGION all
  FLOW_CONDITION initial
END

BOUNDARY_CONDITION left_end
  REGION left_end
  FLOW_CONDITION left_end
END

BOUNDARY_CONDITION right_end
  FLOW_CONDITION right_end
  REGION right_end
END
"""
  
  
f.write(simulation)
f.write("SUBSURFACE\n")
f.write(grid)
f.write(region)
f.write(mat_prop)
f.write(char_curves)
f.write(eoswater)
f.write(fluid_prop)
f.write(linear_solver)
f.write(strata)
f.write(time)
f.write(output)
f.write(flow_cond)
f.write(init_cond)
f.write("\nEND_SUBSURFACE\n") 
# Must have \n at the end, or HDF5 output will not work