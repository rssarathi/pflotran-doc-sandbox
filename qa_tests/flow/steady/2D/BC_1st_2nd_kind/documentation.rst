.. _flow-steady-2D-pressure-BC-1st-2nd-kind:

************************************************
2D Steady Flow (Pressure), BCs of 1st & 2nd Kind
************************************************
:ref:`flow-steady-2D-pressure-BC-1st-2nd-kind-description`

:ref:`flow-general-steady-2D-pressure-BC-1st-2nd-kind-pflotran-input`

:ref:`flow-th-steady-2D-pressure-BC-1st-2nd-kind-pflotran-input`

:ref:`flow-richards-steady-2D-pressure-BC-1st-2nd-kind-pflotran-input`

:ref:`flow-steady-2D-pressure-BC-1st-2nd-kind-dataset`

:ref:`flow-steady-2D-pressure-BC-1st-2nd-kind-python`

.. _flow-steady-2D-pressure-BC-1st-2nd-kind-description:

The Problem Description
=======================

This problem is adapted from *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.2.4, pg.29, "A 2D Steady-State 
Pressure Distribution, Boundary Conditions of 1st and 2nd Kind."

The domain is a 2x1x1 meter rectangular slab extending along the positive 
x- and y-axis and is made up of 20x10x1 hexahedral grid cells with dimensions 
0.1x0.1x1 meters. The domain is composed of a single material and is assigned 
the following properties: permeability *k* = 1e-14 m^2; rock density = 
2,800 kg/m^3; fluid density = 1,000 kg/m^3; fluid viscosity :math:`\mu` = 
1 mPa-s; porosity = 0.50.

The pressure is initially uniform at *p(t=0)* = 1.0 MPa.
The pressure boundary conditions (in units of MPa) and fluid flux boundary
conditions (in units of m/s) are:

.. math::
   p(x,0) = p_0 {x \over L}            \hspace{0.25in} y=0 \hspace{0.15in} face
   
   p(x,L) = p_0 {{x+2L} \over L}       \hspace{0.25in} y=L \hspace{0.15in} face
   
   p(2L,y) = p_0 {{2y+2L} \over L}     \hspace{0.25in} x=2L \hspace{0.15in} face
   
   q(0,y) = {{-k}\over{\mu}}{p_0 \over L} = -10^{-5} \hspace{0.25in} x=0 \hspace{0.15in} face

where L = 1 m and p\ :sub:`0` = 1 MPa. The simulation is run until the 
steady-state pressure distribution develops. 

The LaPlace equation governs the steady-state pressure distribution,

.. math:: {{\partial^{2} p} \over {\partial x^{2}}} + {{\partial^{2} p} \over {\partial y^{2}}} = 0

When the boundary conditions are applied, the solution becomes,

.. math:: 
   p(x,y) = {p_0 \over L} (x+2y)

.. figure:: ../qa_tests/flow/steady/2D/BC_1st_2nd_kind/visit_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/flow/steady/2D/BC_1st_2nd_kind/general_mode/comparison_plot.png
   :width: 49 %   
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for GENERAL mode.
   
.. figure:: ../qa_tests/flow/steady/2D/BC_1st_2nd_kind/th_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for TH mode.

.. figure:: ../qa_tests/flow/steady/2D/BC_1st_2nd_kind/richards_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for RICHARDS mode.

   
.. _flow-general-steady-2D-pressure-BC-1st-2nd-kind-pflotran-input:

The PFLOTRAN Input File (GENERAL Mode)
======================================
The GENERAL Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/steady/2D/BC_1st_2nd_kind/general_mode/2D_steady_pressure_BC_1st_2nd_kind.in>`.

.. literalinclude:: ../qa_tests/flow/steady/2D/BC_1st_2nd_kind/general_mode/2D_steady_pressure_BC_1st_2nd_kind.in



.. _flow-th-steady-2D-pressure-BC-1st-2nd-kind-pflotran-input:

The PFLOTRAN Input File (TH Mode)
=================================
The TH Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/steady/2D/BC_1st_2nd_kind/th_mode/2D_steady_pressure_BC_1st_2nd_kind.in>`.

.. literalinclude:: ../qa_tests/flow/steady/2D/BC_1st_2nd_kind/th_mode/2D_steady_pressure_BC_1st_2nd_kind.in



.. _flow-richards-steady-2D-pressure-BC-1st-2nd-kind-pflotran-input:

The PFLOTRAN Input File (RICHARDS Mode)
=======================================
The RICHARDS Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/steady/2D/BC_1st_2nd_kind/richards_mode/2D_steady_pressure_BC_1st_2nd_kind.in>`.

.. literalinclude:: ../qa_tests/flow/steady/2D/BC_1st_2nd_kind/richards_mode/2D_steady_pressure_BC_1st_2nd_kind.in



.. _flow-steady-2D-pressure-BC-1st-2nd-kind-dataset:

The Dataset
===========
The hdf5 dataset required to define the initial/boundary conditions is created
with the following python script called ``create_dataset.py``:

.. literalinclude:: ../qa_tests/flow/steady/2D/BC_1st_2nd_kind/create_dataset.py


  
.. _flow-steady-2D-pressure-BC-1st-2nd-kind-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: flow_steady_2D_BC1st2ndkind
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.
