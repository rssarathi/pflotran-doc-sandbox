.. _flow-steady-1D-pressure-BC-1st-kind:

******************************************
1D Steady Flow (Pressure), BCs of 1st Kind
******************************************
:ref:`flow-steady-1D-pressure-BC-1st-kind-description`

:ref:`flow-general-steady-1D-pressure-BC-1st-kind-pflotran-input`

:ref:`flow-th-steady-1D-pressure-BC-1st-kind-pflotran-input`

:ref:`flow-richards-steady-1D-pressure-BC-1st-kind-pflotran-input`

:ref:`flow-steady-1D-pressure-BC-1st-kind-python`



.. _flow-steady-1D-pressure-BC-1st-kind-description:

The Problem Description
=======================

This problem is adapted from *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.2.1, pg.27, "A 1D Steady-State 
Pressure Distribution, Boundary Conditions of 1st Kind."

The domain is a 100x10x10 meter rectangular beam extending along the positive 
x-axis and is made up of 10x1x1 cubic grid cells with dimensions 10x10x10 
meters. The materials are assigned the following properties: porosity = 0.5; 
fluid viscosity :math:`\mu` = 1.0 mPa-s; permeability = 1e-15 m^2; 
fluid density *rho* = 1,000 kg/m^3.

The pressure is initially uniform at *p(t=0)* = 1.5 MPa.
The boundary pressures are *p(x=0)* = 2.0 MPa and *p(x=L)* = 1.0 MPa, 
where L = 100 m.
The simulation is run until the steady-state pressure distribution
develops. 

The LaPlace equation governs the steady-state pressure distribution,

.. math:: {{\partial^{2} p} \over {\partial x^{2}}} = 0

The solution is given by,

.. math:: p(x) = ax + b

When the boundary conditions *p(x=0)* = 2.0 MPa and *p(x=L)* = 1.0 MPa are 
applied, the solution becomes,

.. math:: p(x) = {-x \over L} + 2.0

.. figure:: ../qa_tests/flow/steady/1D/BC_1st_kind/visit_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/flow/steady/1D/BC_1st_kind/general_mode/comparison_plot.png
   :width: 49 %  
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for GENERAL mode.
   
.. figure:: ../qa_tests/flow/steady/1D/BC_1st_kind/th_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for TH mode.
   
.. figure:: ../qa_tests/flow/steady/1D/BC_1st_kind/richards_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for RICHARDS mode.
   
   
   
.. _flow-general-steady-1D-pressure-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (GENERAL Mode)
======================================
The General Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/steady/1D/BC_1st_kind/general_mode/1D_steady_pressure_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/flow/steady/1D/BC_1st_kind/general_mode/1D_steady_pressure_BC_1st_kind.in



.. _flow-th-steady-1D-pressure-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (TH Mode)
=================================
The TH Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/steady/1D/BC_1st_kind/th_mode/1D_steady_pressure_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/flow/steady/1D/BC_1st_kind/th_mode/1D_steady_pressure_BC_1st_kind.in



.. _flow-richards-steady-1D-pressure-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (RICHARDS Mode)
=======================================
The RICHARDS Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/steady/1D/BC_1st_kind/richards_mode/1D_steady_pressure_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/flow/steady/1D/BC_1st_kind/richards_mode/1D_steady_pressure_BC_1st_kind.in


  
.. _flow-steady-1D-pressure-BC-1st-kind-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: flow_steady_1D_BC1stkind
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.
