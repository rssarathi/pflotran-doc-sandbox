.. _thermal-steady-1D-conduction-BC-1st-kind:

*********************************************
1D Steady Thermal Conduction, BCs of 1st Kind 
*********************************************
:ref:`thermal-steady-1D-conduction-BC-1st-kind-description`

:ref:`thermal-general-steady-1D-conduction-BC-1st-kind-pflotran-input`

:ref:`thermal-th-steady-1D-conduction-BC-1st-kind-pflotran-input`

:ref:`thermal-steady-1D-conduction-BC-1st-kind-python`



.. _thermal-steady-1D-conduction-BC-1st-kind-description:

The Problem Description
=======================

This problem is adapted from *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.1.1, pg.14, "A 1D Steady-State 
Temperature Distribution, Boundary Conditions of 1st Kind."

The domain is a 100x10x10 meter rectangular beam extending along the positive 
x-axis and is made up of 10x1x1 cubic grid cells with dimensions 10x10x10 
meters. The domain material is assigned the following properties: thermal
conductivity *K* = 1.0 W/(m-C); specific heat capacity *Cp* = 0.001 J/(m-C); 
density *rho* = 2,800 kg/m^3.

The temperature is initially uniform at *T(t=0)* = 1.5C.
The boundary temperatures are *T(x=0)* = 1.0C and *T(x=L)* = 2.0C, where L = 100 m.
The simulation is run until the steady-state temperature distribution
develops. 

The LaPlace equation governs the steady-state temperature distribution,

.. math:: {{\partial^{2} T} \over {\partial x^{2}}} = 0

The solution is given by,

.. math:: T(x) = ax + b

When the boundary conditions *T(x=0)* = 1.0C and *T(x=L)* = 2.0C are applied,
the solution becomes,

.. math:: T(x) = T(x=0){x \over L}

.. figure:: ../qa_tests/thermal/steady/1D/BC_1st_kind/visit_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/thermal/steady/1D/BC_1st_kind/general_mode/comparison_plot.png
   :width: 49 %  
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for GENERAL mode.
   
.. figure:: ../qa_tests/thermal/steady/1D/BC_1st_kind/th_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for TH mode.
   
   
   
.. _thermal-general-steady-1D-conduction-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (GENERAL Mode)
======================================
The General Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/thermal/steady/1D/BC_1st_kind/general_mode/1D_steady_thermal_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/thermal/steady/1D/BC_1st_kind/general_mode/1D_steady_thermal_BC_1st_kind.in



.. _thermal-th-steady-1D-conduction-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (TH Mode)
=================================
The TH Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/thermal/steady/1D/BC_1st_kind/th_mode/1D_steady_thermal_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/thermal/steady/1D/BC_1st_kind/th_mode/1D_steady_thermal_BC_1st_kind.in


  
.. _thermal-steady-1D-conduction-BC-1st-kind-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: thermal_steady_1D_BC1stkind
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.
