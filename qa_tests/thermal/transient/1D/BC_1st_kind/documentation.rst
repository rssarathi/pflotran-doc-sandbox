.. _thermal-transient-1D-conduction-BC-1st-kind:

************************************************
1D Transient Thermal Conduction, BCs of 1st Kind 
************************************************
:ref:`thermal-transient-1D-conduction-BC-1st-kind-description`

:ref:`thermal-general-transient-1D-conduction-BC-1st-kind-pflotran-input`

:ref:`thermal-th-transient-1D-conduction-BC-1st-kind-pflotran-input`

:ref:`thermal-transient-1D-conduction-BC-1st-kind-python`



.. _thermal-transient-1D-conduction-BC-1st-kind-description:

The Problem Description
=======================

This problem is adapted from *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.1.6, pg.19, "A Transient 1D 
Temperature Distribution, Time-Dependent Boundary Conditions of 1st Kind."

The domain is a 20x1x1 meter rectangular column extending along the positive 
and negative x-axis and is made up of 20x1x1 cubic grid cells with dimensions 
1x1x1 meters. The domain material is assigned the following properties: thermal
conductivity *K* = 2.0 W/(m-C); specific heat capacity *Cp* = 1.5 J/(m-C); 
density *rho* = 2,500 kg/m^3.

The temperature is initially uniform at *T(t=0)* = 0 C.
The boundary temperatures are:

.. math::
   T(-L,t) = T_b t
   
   T(L,t) = T_b t

where L = 10 m and :math:`T_b` = 2 C/day. The transient temperature 
distribution is governed by,

.. math:: 
   \rho c_p {{\partial T} \over {\partial t}} = K {{\partial^{2} T} \over {\partial x^{2}}}

With the given boundary conditions, the solution is defined by,

.. math::
   \chi = {K \over {\rho c_p}}

   T(x,t) = T_b t + {{T_b(x^2-L^2)}\over{2\chi}} + {{16T_bL^2}\over{\chi\pi^3}} \sum_{n=0}^{\infty}{{(-1)^n}\over{(2n+1)^3}} cos{\left({{(2n+1)x\pi}\over{2L}}\right)} e^{\left({-\chi(2n+1)^2\pi^2{{t}\over{4L^2}}}\right)}

.. figure:: ../qa_tests/thermal/transient/1D/BC_1st_kind/visit_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/thermal/transient/1D/BC_1st_kind/general_mode/comparison_plot.png
   :width: 49 %  
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for GENERAL mode.
   
.. figure:: ../qa_tests/thermal/transient/1D/BC_1st_kind/th_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for TH mode.
   
   
   
.. _thermal-general-transient-1D-conduction-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (GENERAL Mode)
======================================
The General Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/thermal/transient/1D/BC_1st_kind/general_mode/1D_transient_thermal_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/thermal/transient/1D/BC_1st_kind/general_mode/1D_transient_thermal_BC_1st_kind.in



.. _thermal-th-transient-1D-conduction-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (TH Mode)
=================================
The TH Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/thermal/transient/1D/BC_1st_kind/th_mode/1D_transient_thermal_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/thermal/transient/1D/BC_1st_kind/th_mode/1D_transient_thermal_BC_1st_kind.in


  
.. _thermal-transient-1D-conduction-BC-1st-kind-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: thermal_transient_1D_BC1stkind
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.
