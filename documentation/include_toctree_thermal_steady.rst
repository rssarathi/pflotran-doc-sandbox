
.. A list of files you want to include for the tutorials.
   It must be done like this because of the relative paths.
   If you don't need relative paths, then you don't have to do it like this.
   
   
.. THERMAL QA TESTS

.. STEADY
   
.. include:: ../qa_tests/thermal/steady/1D/BC_1st_kind/documentation.rst

.. include:: ../qa_tests/thermal/steady/1D/BC_1st_2nd_kind/documentation.rst

.. include:: ../qa_tests/thermal/steady/2D/BC_1st_kind/documentation.rst

.. include:: ../qa_tests/thermal/steady/2D/BC_1st_2nd_kind/documentation.rst

.. include:: ../qa_tests/thermal/steady/3D/BC_1st_kind/documentation.rst
